<?php namespace App\Models;

use DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Upload extends Model{

    public function setFiles($folderName, $fileName)
    {
        if (!Schema::hasTable($folderName)) {
            Schema::create($folderName, function ($table) {
                $table->increments('id');
                $table->string('file_name')->default('');
                $table->timestamps();
            });
        }

        DB::table($folderName)->insert([
            'file_name' => $fileName,
            'created_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
    }

    public function getAllFiles(){
        $listDB = $this->getAllTables();
        $files = array();
        foreach($listDB as $item){  
            $files[current($item)] =  DB::table(current($item))->get();
        }
        return $files;
    }

    public function getAllTables(){
        return DB::select('SHOW TABLES');
    }

    public function deleteFile($file_id, $folder_name){
        return DB::table($folder_name)->where('id', $file_id)->delete();
    }

}
