<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', ['as' => 'home', 'uses' => 'UploadController@index']);
post('/', ['as' => 'upload', 'uses' => 'UploadController@upload']);

get('/files', ['as' => 'list-files', 'uses' => 'UploadController@listFiles']);
post('/files', ['as' => 'drop-file', 'uses' => 'UploadController@dropFile']);

