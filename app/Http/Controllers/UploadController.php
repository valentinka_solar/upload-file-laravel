<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Upload;

class UploadController extends Controller {

    public function index(Upload $uploadModel)
    {
        return view('upload');
    }
    public function upload(Upload $uploadModel, Request $request)
    {
        $this->validate($request, [
            //'textFile' => 'required|mimes:csv,txt',
        ]);

        $path = base_path() . '/public/images-upload/';
        $pathFolder = $path . 'folder_';
        $filename = uniqid() . '.' . $request->file('textFile')->getClientOriginalExtension();
        $countFolder = ($this->getCountFolders($path) != 0) ? $this->getCountFolders($path) : 1;  // �������� ���-�� �������� � �����

        if(!is_dir($pathFolder . $countFolder)){
            mkdir($pathFolder . $countFolder, 0700);
        }
        $countFile = $this->getCountFiles($pathFolder . $countFolder);  // �������� ���-�� ������ � �����

        if($countFile >= 10){
            $countFolder ++;
            mkdir($pathFolder . $countFolder, 0700);
        }

        $request->file('textFile')->move($path . 'folder_' . $countFolder, $filename);
        $uploadModel->setFiles('folder_' . $countFolder, $filename);

        if ($request->hasFile('textFile')) {
            return redirect('/files')->with('message', 'File was added!');
        }

    }

    public function dropFile(Upload $uploadModel, Request $request)
    {
        $file_id = $request->get('id');
        $folder_name = $request->get('folder');
        $file_name = $request->get('file');

        if($uploadModel->deleteFile($file_id, $folder_name)){
            unlink(base_path() . '/public/images-upload/' . $folder_name . '/' .$file_name );
            //return redirect('/files')->with('message', 'File was deleted!');
        }
    }

    public function listFiles(Upload $uploadModel)
    {
        $files = $uploadModel->getAllFiles();
        return view('list', ['files' => $files]);
    }

    public function getCountFiles($path)
    {
        $dir = opendir($path);
        $count = 0;
        while($file = readdir($dir)){
            if(strpos($file, '.txt', 1) ){
                $count++;
            }
        }
        return $count;
    }

    public function getCountFolders($path)
    {
        $dir_list = scandir($path);
        $count = 0;
        foreach($dir_list as $dr)
        {
            if ($dr!='.' AND $dr!='..')
            {
                if (is_dir($path."/".$dr)){
                    $count++;
                }
            }
        }
        return $count;
    }



}
