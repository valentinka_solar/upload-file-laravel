@extends('app')

@section('content')
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <a href="/">Also you can upload file</a>
    <br/><br/>
    @foreach($files as $key => $file)
        <ul class="list-group col-xs-12 col-sm-6 ">
            <li class="list-group-item disabled">{{ $key }}</li>
             @foreach($file as $item)
                <li class="list-group-item ">
                    <a href="{{ URL::to( '/images-upload/' . $key . '/' . $item->file_name )  }}" class="text-left" target="_blank  ">
                        {{ $item->file_name }}
                    </a>
                    <a href="#" class="text-right remove-link remove-file"
                       data-id="{{ $item->id }}"
                       data-folder="{{ $key }}"
                       data-file="{{ $item->file_name }}"
                       title="Delite">X</a>
                </li>
              @endforeach
        </ul>
    @endforeach


@endsection