@extends('app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
<h2>Upload txt file</h2>
{!! Form::open(array(
                'url' => '/',
                'method' => 'post',
                'class' => 'form',
                'id' => 'mainForm',
                'files' => true
              ))
 !!}
    <div class="controls clearfix">
        <span class="btn btn-success btn-file">
            <i class="icon-plus"></i> <span>Choose txt file...</span>
            {!! Form::file('textFile', array(
                    'id' => 'uploadTxt',
                    'accept' => 'text/plain',
                    'required' => 'required'
            )) !!}
        </span>
    </div>
    <br/>
    <div class="controls clearfix">
        {!! Form::submit('Upload', array(
                  'id' => 'upload' ,
                  'class' => 'btn btn-default'
        )) !!}
    </div>
{!! Form::close() !!}
<br/>
<a href="/files">Also you can see all files</a>
@endsection