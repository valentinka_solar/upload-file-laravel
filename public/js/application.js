$(document).ready(function() {
    $('#main').slideDown('slow');
    

    $('.btn-file').each(function() {
        var self = this;
        $('input[type=file]', this).change(function() {
            $(self).next().remove();
            var value = $(this).val();
            var fileName = value.substring(value.lastIndexOf('/') + 1);
            var fileExt = fileName.split('.').pop().toLowerCase();
            $('<span><i class="icon-file icon-' + fileExt + '"></i> ' + fileName + '</span>').insertAfter(self);
        });
    });


    $('a.remove-file').click(function(e){
        e.preventDefault();
        var choise = confirm('Are you sure you want to delete this item?');
        var csrftoken = $("meta[name='csrf-token']").attr('content');
        if(choise){
            $.ajax({
                url: '/files',
                type: 'post',
                data: {
                    id      : $(this).data('id'),
                    folder  : $(this).data('folder'),
                    file  : $(this).data('file')
                },
                beforeSend: function(request) {
                    return request.setRequestHeader('X-CSRF-Token', csrftoken);
                },
                success: function(result){
                    window.location.reload();
                }
            });
        }
    });


    $('#upload').click(function(e) {
        var data = new FormData($('#mainForm')[0]);
        $.ajax({
            type: "POST",
            url: "/upload.php",
            data: data,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#loader').show();
            }
        }).done(function(html) {
            $("#results").append(html);
            $('#loader').hide();
            $('#mainForm')[0].reset();
            $('#uploadImage').val('');
            
        });

    });

     

});
